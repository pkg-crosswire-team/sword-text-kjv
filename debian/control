Source: sword-text-kjv
Section: text
Priority: optional
Maintainer: CrossWire Packaging Team <pkg-crosswire-devel@alioth-lists.debian.net>
Uploaders: Roberto C. Sanchez <roberto@debian.org>,
           Daniel Glassey <wdg@debian.org>,
	   Dominique Corbex <dominique@corbex.org>,
           Teus Benschop <teusjannette@gmail.com>
Homepage: https://ebible.org/eng-kjv2006/
Vcs-Browser: https://salsa.debian.org/pkg-crosswire-team/sword-text-kjv
Vcs-Git: https://salsa.debian.org/pkg-crosswire-team/sword-text-kjv.git
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13), libsword-utils, u2o
Standards-Version: 4.5.1

Package: sword-text-kjv
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: sword-dict-strongs-greek, sword-dict-strongs-hebrew, sword-comm
Provides: sword-text
Enhances: sword-frontend
Description: King James Version with Strongs Numbers for SWORD
 This is the King James Version of the Holy Bible (also known as the
 Authorized Version) with embedded Strong's Numbers.  These mechanisms provide
 a useful means for looking up the exact original language word in a lexicon
 that is keyed to Strong's numbers.
 .
 This free text of the King James Version of the Holy Bible is brought to you
 courtesy of the Crosswire Bible Society and eBible.org.
 .
 This package requires a libsword version of at least 1.7.0 to access
 through a SWORD front end.  However, this is not declared as a dependency
 because you may have occasion to install just the text without any sort
 of front end or other interface.
